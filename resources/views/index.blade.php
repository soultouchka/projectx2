@extends('layouts.master')
@section('title', 'Главная страница')
@section('content')

    {{--Все категорий, регионы и поиск--}}
    @include('layouts.search_form')

    <section id="last__ads" class="last__ads py-4">
        <p class="h3 font-weight-normal">Последние обьявления</p>
        <div class="row justify-content-around">
            @foreach($ads as $ad)
                @include('layouts.ad_card', compact('ad'))
            @endforeach
        </div>
        <div class="d-flex justify-content-center">
            {{ $ads->links() }}
        </div>
    </section>

    <section id="best__works" class="best__works py-4">
        <p class="h3 font-weight-normal">Лучшие работы</p>
        <div class="row justify-content-around">

            <div class="card border border-info mb-lg-3" style="width: 17rem;">
                <img class="card-img-top" src="img/s3.jpg" alt="Card image cap">
                <div class="card-body">
                    <div class="d-flex">
                        <h5 class="card-title mr-auto">Card title</h5>
                        <i class="fa fa-star-o fMasmaa-2x"></i>
                    </div>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                    <a href="#" class="stretched-link"></a>
                </div>
            </div>
            <div class="card border border-info mb-lg-3" style="width: 17rem;">
                <img class="card-img-top" src="img/s1.jpg" alt="Card image cap">
                <div class="card-body">
                    <div class="d-flex">
                        <h5 class="card-title mr-auto">Card title</h5>
                        <i class="fa fa-star-o fa-2x"></i>
                    </div>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                    <a href="#" class="stretched-link"></a>
                </div>
            </div>
            <div class="card border border-info mb-lg-3" style="width: 17rem;">
                <img class="card-img-top" src="img/s3.jpg" alt="Card image cap">
                <div class="card-body">
                    <div class="d-flex">
                        <h5 class="card-title mr-auto">Card title</h5>
                        <i class="fa fa-star-o fa-2x"></i>
                    </div>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                    <a href="#" class="stretched-link"></a>
                </div>
            </div>
        </div>
    </section>
@endsection
