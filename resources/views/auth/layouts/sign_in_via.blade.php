<div class="border-left d-flex flex-column px-3">
    <button class="btn btn-outline-info mb-4" type="submit"><i class="fa fa-google"></i> Войти с помощью Google</button>
    <button class="btn btn-outline-info mb-4" type="submit"><i class="fa fa-facebook-f"></i> Войти с помощью Facebook</button>
    <button class="btn btn-outline-info mb-4" type="submit"><i class="fa fa-vk"></i> Войти с помощью Вконтакте</button>
</div>
