@extends('auth.layouts.master')
@section('title', 'Регистрация пользователя')
@section('content')
    <h1>StroyHub</h1>
    <p class="h3 font-weight-normal">Войдите на сайт с помощью электронной почты или через социальную сеть</p>
    <hr class="my-4">
    <div class="row">
        <form method="POST" action="{{route('register')}}" class="col-lg-6">
            @csrf
            <div class="form-group">
                <label for="name">Имя*</label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Электронная почта*</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                <small id="emailHelp" class="form-text text-muted">Мы никогда никому не передадим вашу электронную почту.</small>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">Пароль*</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password-confirm">Повторите пароль*</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
            <div class="form-group">
                <label for="exampleInputKey">Результат с картинки*</label>
                <div class="row">
                    <input type="text" class="ml-3 form-control col-lg-2" id="exampleInputKey" placeholder="">
                    <img src="img/g2.png" alt="">
                </div>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1"><small class="form-text text-muted">Я соглашаюсь с правилами использования сервиса, а также с передачей и обработкой моих данных.*</small></label>
            </div>
            <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
        </form>
        @include('auth.layouts.sign_in_via');
    </div>
    <div class="my-5 col-lg-4">
        <div class="row">
            <p class="mr-2">Вы уже зарегистрированы?</p>
            <a href="{{route('login')}}">Войдите на сайт</a>
        </div>
        <div class="row">
            <p class="mr-2">Забыли пароль?</p>
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                   Востановить
                </a>
            @endif
        </div>
    </div>
@endsection
