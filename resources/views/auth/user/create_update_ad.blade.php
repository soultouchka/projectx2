@extends('layouts.master')

@isset($ad)
    @section('title', 'Редактировать объявление ' . $ad->title)
@else
    @section('title', 'Добавить объявление')
@endisset

@section('content')
    <section id="add_an_ad" class="add_an_ad my-3">
        @isset($ad)
            <p class="h3 font-weight-normal">Редактировать объявление: {{$ad->title}}</p>
        @else
            <p class="h3 font-weight-normal">Добавить объявлениe</p>
        @endisset
        <form method="post" enctype="multipart/form-data" class="col-lg-8"
              @isset($ad)
              action="{{route('ads.update', $ad)}}"
              @else
              action="{{route('ads.store')}}"
            @endisset>
            @csrf
            @isset($ad)
                @method('PUT')
            @endisset
            <div class="form-group">
                <label for="exampleFormControlInput1">Заголовок</label>
                @include('auth.layouts.error', ['fieldName' => 'title'])
                <input type="text" name="title" class="form-control" id="exampleFormControlInput1" value="{{ old('title', isset($ad) ? $ad->title : null) }}">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Выберите категорию</label>
                @include('auth.layouts.error', ['fieldName' => 'category_id'])
                <select name="category_id" class="form-control" id="exampleFormControlSelect1">
                    <option disabled selected value="" >Выберите категорию</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}"
                            @isset($ad)
                                @if($ad->category_id == $category->id)
                                    selected
                                @endif
                            @endisset
                        >{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Описание</label>
                @include('auth.layouts.error', ['fieldName' => 'description'])
                <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="10">
                    {{ old('description', isset($ad) ? $ad->description : null) }}
                </textarea>
            </div>
            <div class="form-group">
                <label for="customFile">Фотографии</label>
                <div class="custom-file mb-2">
                    <input type="file" name="head_image" class="custom-file-input form-control" id="customHeadFile" value="@isset($ad){{$ad->head_photo}}@endisset">
                    <label class="custom-file-label" for="customHeadFile">Выберите главное фото</label>
                </div>
                <div class="custom-file">
                    <input type="file" name="images[]" multiple class="custom-file-input form-control" id="customFile">
                    <label class="custom-file-label" for="customFile">Выберите фотографии</label>
                </div>
            </div>
            <!--
            <div class="form-group">
              <button type="submit" name="upload" value="upload" id="upload" class="btn btn-block btn-dark"><i class="fa fa-fw fa-upload"></i> Загрузить</button>
            </div>
            -->
            <div class="form-group">
                <label for="exampleInputAddress">Добавьте населенный пункт*</label>
                @include('auth.layouts.error', ['fieldName' => 'address'])
                <input name="address" type="text" class="form-control" id="exampleInputAddress" aria-describedby="AddressHelp"value="{{ old('address', isset($ad) ? $ad->address : null) }}">
            </div>
            <div class="form-group">
                <label for="exampleInputPhone">Номер телефона</label>
                @include('auth.layouts.error', ['fieldName' => 'phone_number'])
                <input name="phone_number" type="text" class="form-control" id="exampleInputPhone" value="{{ old('phone_number', isset($ad) ? $ad->phone_number : null) }}">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail">Email</label>
                <input name="email" type="email" class="form-control" id="exampleInputEmail" value="{{ old('email', isset($ad) ? $ad->email : null) }}">
            </div>
            <div class="form-group">
                <label for="exampleInputName">Контактное лицо*</label>
                <input name="owner_name" type="text" class="form-control" id="exampleInputName" value="{{ old('owner_name', isset($ad) ? $ad->owner_name : $current_user->name) }}">
            </div>
            <button type="submit" class="btn btn-primary">Опубликовать</button>
        </form>
    </section>
@endsection
