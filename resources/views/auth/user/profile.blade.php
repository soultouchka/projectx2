@extends('layouts.master')
@section('title', 'Профиль: ' . $user->name)
@section('content')
<nav>
    <div class="nav nav-tabs mb-2" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-ads-tab" data-toggle="tab" href="#nav-ads" role="tab" aria-controls="nav-ads" aria-selected="true">Мои объявления</a>
        <a class="nav-item nav-link" id="nav-saved-tab" data-toggle="tab" href="#nav-saved" role="tab" aria-controls="nav-saved" aria-selected="false">Избранные</a>
        <a class="nav-item nav-link" id="nav-messages-tab" data-toggle="tab" href="#nav-messages" role="tab" aria-controls="nav-messages" aria-selected="false">Сообщения</a>
        <a class="nav-item nav-link" id="nav-payments-tab" data-toggle="tab" href="#nav-payments" role="tab" aria-controls="nav-payments" aria-selected="false">Счёт 50 ₸</a>
        <a class="nav-item nav-link" id="nav-acc_settings-tab" data-toggle="tab" href="#nav-acc_settings" role="tab" aria-controls="nav-acc_settings" aria-selected="false">Настройки</a>
        <a class="nav-item nav-link" id="nav-b_acc_settings-tab" data-toggle="tab" href="#nav-b_acc_settings" role="tab" aria-controls="nav-b_acc_settings" aria-selected="false">Настройки бизнес аккаунта</a>
        <a class="nav-item nav-link" id="nav-sp_llp-tab" data-toggle="tab" href="#nav-sp_llp" role="tab" aria-controls="nav-sp_llp" aria-selected="false">+ Добавить свой ИП/ТОО</a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-ads" role="tabpanel" aria-labelledby="nav-ads-tab">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    @if( isset($category))
                        {{$category->name}}
                    @else
                        Все категорий
                    @endif
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('user_ads_by_category', ['all_categories'])}}">Все категорий</a>
                    @foreach($categories as $cat)
                        <a class="dropdown-item" href="{{route('user_ads_by_category', [$cat->code])}}">{{$cat->name}}</a>
                    @endforeach
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link active" id="pills-active-tab" data-toggle="pill" href="#pills-active" role="tab" aria-controls="pills-active" aria-selected="true">Active</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-check-tab" data-toggle="pill" href="#pills-check" role="tab" aria-controls="pills-check" aria-selected="false">Check</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-inactive-tab" data-toggle="pill" href="#pills-inactive" role="tab" aria-controls="pills-inactive" aria-selected="false">Inactive</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-active" role="tabpanel" aria-labelledby="pills-active-tab">
                <div class="row justify-content-around">
                    @foreach($user_ads as $ad)
                        @include('layouts.ad_card',  compact('ad'))
                    @endforeach
                </div>
            </div>
            <div class="tab-pane fade" id="pills-check" role="tabpanel" aria-labelledby="pills-check-tab">
                <div class="row justify-content-around">
                    <div class="card border border-warning mb-lg-3" style="width: 17rem;">
                        <span class="badge badge-warning badge__urgently">ТОП</span>
                        <img class="card-img-top" src="img/s1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="d-flex">
                                <h5 class="card-title mr-auto">Card title</h5>
                                <i class="fa fa-bookmark-o fa-2x"></i>
                            </div>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.
                                <br><small class="text-muted">Last updated 3 mins ago</small></p>
                            <a href="#" class="stretched-link"></a>
                        </div>
                    </div>
                    <div class="card border mb-lg-3" style="width: 17rem;">
                        <img class="card-img-top" src="img/s3.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="d-flex">
                                <h5 class="card-title mr-auto">Card title</h5>
                                <i class="fa fa-bookmark-o fa-2x"></i>
                            </div>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.
                                <br><small class="text-muted">Last updated 3 mins ago</small></p>
                            <a href="#" class="stretched-link"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-inactive" role="tabpanel" aria-labelledby="pills-inactive-tab">inactive</div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-saved" role="tabpanel" aria-labelledby="nav-saved-tab">
        <div class="row justify-content-around">
            <div class="card border border-warning mb-lg-3" style="width: 17rem;">
                <span class="badge badge-warning badge__urgently">ТОП</span>
                <img class="card-img-top" src="img/s1.jpg" alt="Card image cap">
                <div class="card-body">
                    <div class="d-flex">
                        <h5 class="card-title mr-auto">Card title</h5>
                        <i class="fa fa-bookmark fa-2x"></i>
                    </div>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.
                        <br><small class="text-muted">Last updated 3 mins ago</small></p>
                    <a href="#" class="stretched-link"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-messages" role="tabpanel" aria-labelledby="nav-messages-tab">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-all_messages-tab" data-toggle="pill" href="#pills-all_messages" role="tab" aria-controls="pills-all_messages" aria-selected="true">Все сообщений</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-fav_messages-tab" data-toggle="pill" href="#pills-fav_messages" role="tab" aria-controls="pills-fav_messages" aria-selected="false">Избранные</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-all_messages" role="tabpanel" aria-labelledby="pills-all_messages-tab">.all_messages..</div>
            <div class="tab-pane fade" id="pills-fav_messages" role="tabpanel" aria-labelledby="pills-fav_messages-tab">.fav_messages..</div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-payments" role="tabpanel" aria-labelledby="nav-payments-tab">..payments.</div>
    <div class="tab-pane fade" id="nav-acc_settings" role="tabpanel" aria-labelledby="nav-acc_settings-tab">..acc_settings.</div>
    <div class="tab-pane fade" id="nav-b_acc_settings" role="tabpanel" aria-labelledby="nav-b_acc_settings-tab">..b_acc_settings.</div>
    <div class="tab-pane fade" id="nav-sp_llp" role="tabpanel" aria-labelledby="nav-sp_llp-tab">.sp_llp..</div>
</div>
@endsection
