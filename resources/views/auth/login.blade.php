@extends('auth.layouts.master')
@section('title', 'Войти на сайт')
@section('content')
    <h1>StroyHub</h1>
    <p class="h3 font-weight-normal">Войдите на сайт с помощью электронной почты или через социальную сеть</p>
    <hr class="my-4">
    <div class="row">
        <form method="POST" action="{{ route('login') }}" class="col-lg-6">
            @csrf
            <div class="form-group">
                <label for="email">Электронная почта*</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                       value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <small id="emailHelp" class="form-text text-muted">Мы никогда никому не передадим вашу электронную
                    почту.</small>
            </div>

            <div class="form-group">
                <label for="password">Пароль*</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                       name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember"
                       id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>

            <button type="submit" class="btn btn-primary">
                Войти
            </button>
        </form>

        @include('auth.layouts.sign_in_via');
    </div>
    <div class="my-5 col-lg-4">
        <div class="row">
            <p class="mr-2">Впервые на нашем сайте?</p>
            <a href="{{route('register')}}">Зарегистрируйтесь</a>
        </div>
        <div class="row">
            <p class="mr-2">Забыли пароль?</p>
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Восстановить
                </a>
            @endif
        </div>
    </div>
@endsection
