<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="{{ asset('js/main.js') }}"></script>
    <title>@yield('title')</title>

</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{route('index')}}">StroyHub</a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mx-auto mt-2 mt-lg-0 w-100 nav-justified">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('how_is_it_made')}}">Как это сделано?</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Лучшие работы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('ads')}}">Обьявления</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('ads.create')}}">Добавить объявление</a>
                </li>
                @guest()
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('login')}}">Войти</a>
                    </li>
                @endguest
                @auth()
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('profile')}}">{{Auth::user()->name}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('get-logout')}}">Выйти</a>
                    </li>
                @endauth
            </ul>
        </div>
    </nav>
    <!--Main functianlity-->
    @if(session()->has('success'))
        <p class="alert alert-success">{{ session()->get('success') }}</p>
    @elseif(session()->has('warning'))
        <p class="alert alert-warning">{{ session()->get('warning') }}</p>
    @endif
    @yield('content')

    <!--End of Main functianlity-->
    <footer id="footer" class="footer py-5">
        <div class="row justify-content-around">
            <div>
                <p><small>© 2020 ТОО "StroyHub" - все права защищены <br> По вопросам рекламы:
                        info@stroyhub.com</small></p>
                <img src="img/g1.png" alt="" style="width:150px;">
            </div>
            <div>
                <ul class="list-unstyled">
                    <li>
                        <a href="{{route('how_is_it_made')}}">Как это сделано?</a>
                    </li>
                    <li>
                        <a href="#">Лучшие работы</a>
                    </li>
                    <li>
                        <a href="{{route('ads')}}">Обьявления</a>
                    </li>
                    <li>
                        <a href="{{route('ads.create')}}">Добавить объявление</a>
                    </li>
                    <li>
                        <a href="#">Войти</a>
                    </li>
                </ul>
            </div>
            <div>
                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Карта сайта</a>
                    </li>
                    <li>
                        <a href="#!">Связаться с администрацией</a>
                    </li>
                    <li>
                        <a href="#!">Пользовательское соглашение</a>
                    </li>
                    <li>
                        <a href="#!">Политика конфиденциальности</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
</div>
</body>
</html>
