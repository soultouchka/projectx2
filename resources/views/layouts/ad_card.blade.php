<div class="card border @if($ad->isTop()) border-warning @else border-info @endif mb-lg-3" style="width: 17rem;">
    @if($ad->isTop())
        <span class="badge badge-warning badge__urgently">ТОП</span>
    @endif
    <img class="card-img-top" src="{{Storage::url($ad->head_photo)}}" alt="Card image cap">
    <div class="card-body">
        <div class="d-flex">
            <h5 class="card-title mr-auto">{{$ad->title}}</h5>
            <i class="fa fa-bookmark-o fa-2x"></i>
        </div>
        <p class="card-text text-truncate" style="max-height: 50px;">{{$ad->description}}
            <br><small class="text-muted">Опубликовано в: {{$ad->created_at}}</small>
        </p>
        <a href="{{route('ad_id', [$ad->category->code, $ad])}}" class="stretched-link"></a>
    </div>
</div>
