<form class="form-inline my-2 my-lg-0">
    <div class="dropdown show">
        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            @if( isset($category))
                {{$category->name}}
            @else
                Все категорий
            @endif
        </a>

        <div class="dropdown-menu dropdown-multicol" aria-labelledby="dropdownMenuLink">
            <div class="dropdown">
                <a class="dropdown-item" href="{{route('ads_by_category', ['all_categories'])}}">Все категорий</a>
                @foreach($categories as $cat)
                    <a class="dropdown-item" href="{{route('ads_by_category', [$cat->code])}}">{{$cat->name}}</a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="dropdown show px-2">
        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Все регионы
        </a>

        <div class="dropdown-menu dropdown-multicol" aria-labelledby="dropdownMenuLink">
            <div class="dropdown-row">
                <a class="dropdown-item" href="#">Oranges</a>
                <a class="dropdown-item" href="#">Bananas</a>
                <a class="dropdown-item" href="#">Apples</a>
            </div>
        </div>
    </div>
    <input class="form-control mr-sm-2 col-lg-6" type="search" placeholder="Поиск объявления" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Поиск</button>
</form>
