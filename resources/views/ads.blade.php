@extends('layouts.master')
@section('title', 'Все обьявления')
@section('content')

    @include('layouts.search_form')

    <section id="last__ads" class="last__ads py-4">
        <p class="h3 font-weight-normal">
            @if(isset($category))
                Все обьявления по категорий: {{$category->name}} {{$category->ads->count()}}
            @else
                Все обьявления
            @endif
        </p>
        <div class="row justify-content-around">
            @foreach($ads as $ad)
                @include('layouts.ad_card')
            @endforeach
        </div>
    </section>
@endsection
