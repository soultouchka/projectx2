@extends('layouts.master')
@section('title', 'Как это сделано?')
@section('content')
    <section id="how__to" class="how__to py-4">
        <p class="h3 font-weight-normal">Как это сделано?</p>
        <div class="media border border-info p-2 rounded mb-2">
            <img class="media-left card-img" alt="Image" src="img/s1.jpg">
            <div class="media-body ml-2">
                <h4 class="card-title"><a href="#">Title</a> <span class="badge badge-info">Видео инструкция</span></h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente quasi tempore
                    vitae odit dignissimos ut ad voluptate. Excepturi et necessitatibus beatae, nobis placeat voluptas,
                    cupiditate, inventore consectetur neque eum cum voluptates nisi. Ab quia suscipit tempore inventore,
                    ex dolore, magni! ipsum dolor sit amet, consectetur adipisicing elit. Nemo rerum, harum sapiente
                    expedita nostrum et. Pariatur assumenda veritatis, aliquid qui.</p>
            </div>
        </div>
        <div class="media border border-info p-2 rounded mb-2">
            <img class="media-left card-img" alt="Image" src="img/s2.jpg">
            <div class="media-body ml-2">
                <h4 class="card-title"><a href="#">Title</a></h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente quasi tempore
                    vitae odit dignissimos ut ad voluptate. Excepturi et necessitatibus beatae, nobis placeat voluptas,
                    cupiditate, inventore consectetur neque eum cum voluptates nisi. Ab quia suscipit tempore inventore,
                    ex dolore, magni! ipsum dolor sit amet, consectetur adipisicing elit. Nemo rerum, harum sapiente
                    expedita nostrum et. Pariatur assumenda veritatis, aliquid qui.</p>
            </div>
        </div>
        <div class="media border border-info p-2 rounded mb-2">
            <img class="media-left card-img" alt="Image" src="img/s3.jpg">
            <div class="media-body ml-2">
                <h4 class="card-title"><a href="#">Title</a></h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente quasi tempore
                    vitae odit dignissimos ut ad voluptate. Excepturi et necessitatibus beatae, nobis placeat voluptas,
                    cupiditate, inventore consectetur neque eum cum voluptates nisi. Ab quia suscipit tempore inventore,
                    ex dolore, magni! ipsum dolor sit amet, consectetur adipisicing elit. Nemo rerum, harum sapiente
                    expedita nostrum et. Pariatur assumenda veritatis, aliquid qui.</p>
            </div>
        </div>
    </section>
@endsection
