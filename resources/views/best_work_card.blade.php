<div class="card border border-info mb-lg-3" style="width: 17rem;">
    <img class="card-img-top" src="img/s1.jpg" alt="Card image cap">
    <div class="card-body">
        <div class="d-flex">
            <h5 class="card-title mr-auto">Card title</h5>
            <i class="fa fa-star-o fa-2x"></i>
        </div>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
            card's content.</p>
        <a href="#" class="stretched-link"></a>
    </div>
</div>
