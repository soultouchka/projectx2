@extends('layouts.master')
@section('title', 'User_name')
@section('content')
    <h2>{{$user->name}}</h2>
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-ads-tab" data-toggle="tab" href="#nav-ads" role="tab" aria-controls="nav-ads" aria-selected="true">Все объявления автора</a>
        <a class="nav-item nav-link" id="nav-works-tab" data-toggle="tab" href="#nav-works" role="tab" aria-controls="nav-works" aria-selected="false">Все работы автора</a>
        <a class="nav-item nav-link" id="nav-how_is_it_made-tab" data-toggle="tab" href="#nav-how_is_it_made" role="tab" aria-controls="nav-nav-how_is_it_made" aria-selected="false">Все работы автора по разделу "Как это сделано?"</a>
    </div>
</nav>
<div class="tab-content mt-3" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-ads" role="tabpanel" aria-labelledby="nav-ads-tab">
        <div class="row justify-content-around">
            @foreach($user_ads as $ad)
                @include('layouts.ad_card', compact('ad'))
            @endforeach
        </div>
    </div>
    <div class="tab-pane fade" id="nav-works" role="tabpanel" aria-labelledby="nav-works-tab">...</div>
    <div class="tab-pane fade" id="nav-how_is_it_made" role="tabpanel" aria-labelledby="nav-how_is_it_made-tab">...</div>
</div>
@endsection
