@extends('layouts.master')
@section('title', 'Обьявление')
@section('content')
    {{--Все категорий, регионы и поиск--}}
    @include('layouts.search_form')

    <section id="all__ad" class="all__ad mt-4">
        <div class="container">
            <div class="row mb-3">
                <div class="ad__images col-lg-8">
                    <div class="main__image">
                        <img src="{{ Storage::url($ad->head_photo) }}" alt="" id="mainImage">
                    </div>
                    <div class="rest__images">
                        <button type="button" class="btn btn-outline-info m-1" onclick="switchImg('img/s1.jpg')"><img
                                src="img/s1.jpg" style="width: 100px;"></button>
                        <button type="button" class="btn btn-outline-info m-1" onclick="switchImg('img/s2.jpg')"><img
                                src="img/s2.jpg" style="width: 100px;"></button>
                        <button type="button" class="btn btn-outline-info m-1" onclick="switchImg('img/s3.jpg')"><img
                                src="img/s3.jpg" style="width: 100px;"></button>
                    </div>
                </div>
                <div class="ad__owner__address col-lg-4">
                    <div class="owner">
                        <div class="d-flex mb-2">
                            <i class="fa fa-user-circle fa-5x mr-2"></i>
                            <div>
                                <h4>{{$ad->owner_name}}</h4>
                                <a href="{{route('user_ads', [$owner->id])}}">Другие обьявления автора</a>
                                @isset($current_user)
                                    @if($owner->id == $current_user->id)
                                        <a class="btn btn-outline-secondary my-2" href="{{route('ads.edit', [$ad])}}">Редактировать объявление</a>
                                        <form action="{{route('ads.destroy', $ad)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-outline-secondary" value="Удалить объявление">
                                        </form>
                                    @endif
                                @endisset
                            </div>
                        </div>
                        <div class="contacts mb-2">
                            <button onclick="showPhoneNum('ХЗ как это сделать')" type="button" class="btn btn-outline-info" id="show__num">
                                Показать номер
                            </button>
                            <button type="button" class="btn btn-outline-secondary">Написать атвору</button>
                        </div>
                    </div>
                    <div class="address">
                        <div class="d-flex">
                            <i class="fa fa-map-marker fa-2x mr-2"></i>
                            <p class="h5 font-weight-normal">{{$ad->address}}</p>
                        </div>
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d92988.42983572344!2d76.88355839999998!3d43.25376!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2skz!4v1597681934629!5m2!1sen!2skz"
                            width="350" height="330" frameborder="0" style="border:0;" allowfullscreen=""
                            aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
            <div class="ad__description col-lg-8">
                @if($ad->isTop())
                    <span class="badge badge-warning">ТОП</span>
                @endif
                <div class="d-flex mb-3">
                    <p class="h4 font-weight-normal mr-auto">{{$ad->title}}</p>
                    <i class="fa fa-bookmark-o fa-2x mr-3"></i>
                    <i class="fa fa-share-alt fa-2x"></i>
                </div>
                <div>
                    <p class="h6 font-weight-normal mb-5">{{$ad->description}}</p>
                    <small class="text-muted mr-5">Опубликовано в {{$ad->created_at}}</small>
                    <small class="text-muted">Просмотры:820</small>
                </div>
            </div>
        </div>
    </section>
@endsection
