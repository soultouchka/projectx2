<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->tinyInteger('is_top')->default(0);
            $table->string('address');
            $table->string('phone_number');
            $table->string('email')->nullable();
            $table->string('owner_name');
            $table->text('head_photo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->dropColumn('is_top');
            $table->dropColumn('address');
            $table->dropColumn('phone_number');
            $table->dropColumn('email');
            $table->dropColumn('owner_name');
            $table->dropColumn('head_photo');
        });
    }
}
