<?php

use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads')->insert([
            'user_id' => 1,
            'category_id' => 20,
            'title' => 'Реклама',
            'description' => 'По вопросам рекламы info@stroyhub.com',
            'is_top' => 1,
            'address' => 'Алматиская обл., г.Талгар',
            'phone_number' => '8 (747) 154 06 09',
            'email' => 'soultouchka@gmail.com',
            'owner_name' => 'Султанбек',
            'head_photo' => null,
        ]);
    }
}
