function showPhoneNum(phone_number) // no ';' here
{
    var elem = document.getElementById("show__num");
    if (elem.innerHTML==="Показать номер") {
        elem.innerHTML = phone_number;
    }
    else elem.innerHTML = "Показать номер";
}
function switchImg(path) {
    document.getElementById('mainImage').src=path;
}

$(document).ready(function() {
    $('input[type="file"]').on("change", function() {
        let filenames = [];
        let files = document.getElementById("customFile").files;
        if (files.length > 8) {
            alert("Вы можете загрузить не больше 8 изображении");
        }
        else if (files.length > 1) {
            filenames.push("Total Files (" + files.length + ")");
        } else {
            for (let i in files) {
                if (files.hasOwnProperty(i)) {
                    filenames.push(files[i].name);
                }
            }
        }
        $(this)
            .next(".custom-file-label")
            .html(filenames.join(","));
    });
});
