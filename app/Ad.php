<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    public function category() {
        return $this->belongsTo(Category::class);
    }

//    public function owner() {
//        return $this->belongsTo(User::class);
//    }

    public function save_update_ad($user_id, $category_id, $title, $description, $address, $phone, $email, $name, $head_photo) {
        $this->user_id = $user_id;
        $this->category_id = $category_id;
        $this->title = $title;
        $this->description = $description;
        $this->address = $address;
        $this->phone_number = $phone;
        $this->email = $email;
        $this->owner_name = $name;
        $this->head_photo = $head_photo;
        $this->save();
        return true;
    }

    public function isTop() {
        return $this->is_top === 1;
    }
}
