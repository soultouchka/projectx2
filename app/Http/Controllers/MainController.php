<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use JavaScript;

class MainController extends Controller
{
    public function index() {
        $ads = Ad::paginate(4);
        $categories = Category::get();
        return view('index', compact('categories', 'ads'));
    }
    public function how_is_it_made() {
        return view('how_is_it_made');
    }
    public function ads() {
        $ads = Ad::get();
        $categories = Category::get();
        return view('ads', compact('categories', 'ads'));
    }
    public function ads_by_category($code) {
        $categories = Category::get();
        $category = Category::where('code', $code)->first();
        if ($code == 'all_categories') {
            $ads = Ad::get();
        }
        else {
            $ads = Ad::where('category_id', $category->id)->get();
        }
        return view('ads', compact('categories', 'category', 'ads'));
    }
    public function ad_id($code, $ad_id) {
        $categories = Category::get();
        $category = Category::where('code', $code)->first();
        $ad = Ad::find($ad_id);
        $owner = User::find($ad->user_id);
        $current_user = Auth::user();
        JavaScript::put([
            'phone_number' => $ad->phone_number
        ]);
        return view('ad', compact('categories', 'category', 'ad', 'owner', 'current_user'));
    }
    public function user_ads($user_id) {
        $user = User::find($user_id);
        $user_ads = $user->ads;

        return view('user_ads_works', compact('user', 'user_ads'));
    }
}
