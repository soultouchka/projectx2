<?php

namespace App\Http\Controllers\Profile;

use App\Ad;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $current_user = Auth::user();
        $categories = Category::get();
        return view('auth.user.create_update_ad', compact('categories', 'current_user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AdRequest $request)
    {
        $head_photo = null;
        if ($request->has('head_image')) {
            $head_photo = $request->file('head_image')->store('ads');
        }
        $ad = new Ad();
        $success = $ad->save_update_ad(Auth::id(), $request->category_id, $request->title, $request->description,
            $request->address, $request->phone_number, $request->email, $request->owner_name, $head_photo);

        if ($success) {
            session()->flash('success', 'Ваше объявление добавлено');
        } else {
            session()->flash('warning', 'Случилась ошибка');
        }
        return redirect()->route('index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad  $ad
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        $categories = Category::get();
        return view('auth.user.create_update_ad', compact('categories', 'ad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ad  $ad
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AdRequest $request, Ad $ad)
    {
        $head_photo = $request->file('head_photo')->store('ads');
        $success = $ad->save_update_ad(Auth::id(), $request->category_id, $request->title, $request->description,
            $request->address, $request->phone_number, $request->email, $request->owner_name, $head_photo);
        if ($success) {
            session()->flash('success', 'Ваше объявление изменено');
        } else {
            session()->flash('warning', 'Случилась ошибка');
        }
        return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad  $ad
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        session()->flash('success', 'Ваше объявление удалено');
        $ad->delete();
        return redirect()->route('index');
    }
}
