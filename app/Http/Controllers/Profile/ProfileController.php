<?php

namespace App\Http\Controllers\Profile;

use App\Ad;
use App\Category;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::get();
        $user = Auth::user();
        $user_ads = $user->ads;
        return view('auth.user.profile', compact('categories', 'user', 'user_ads'));
    }

    public function user_ads_by_category($code) {
        $categories = Category::get();
        $category = Category::where('code', $code)->first();
        if ($code == 'all_categories') {
            $user_ads = Auth::user()->ads;
        }
        else {
            $user_ads = Ad::where('category_id', $category->id)->where('user_id', Auth::id())->get();
        }
        return view('auth.user.profile', compact('categories', 'category', 'user_ads'));
    }
}
