<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|min:3|max:255',
            'category_id' => 'required',
            'description' => 'required|min:3',
            'address' => 'required|min:3|max:255',
            'phone_number' => 'required|min:10|max:22',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => 'Поле заголовок обязательно для ввода',
            'title.min' => 'Поле заголовок должно содержать не менее :min символов',
            'title.max' => 'Поле заголовок должно содержать не более :max символов',
            'category_id.required' => 'Поле категория обязательно для выбора',
            'description.required' => 'Поле описание обязательно для ввода',
            'description.min' => 'Поле описание должно содержать не менее :min символов',
            'address.required' => 'Поле населенный пункт обязательно для ввода',
            'address.min' => 'Поле населенный пункт должно содержать не менее :min символов',
            'address.max' => 'Поле населенный пункт должно содержать не более :max символов',
            'phone_number.required' => 'Поле номер телефона пункт обязательно для ввода',
            'phone_number.min' => 'Поле номер телефона должно содержать не менее :min символов',
            'phone_number.max' => 'Поле номер телефона должно содержать не более :max символов',
        ];
    }
}
