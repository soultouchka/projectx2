<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes([
    'reset' => false,
    'confirm' => false,
    'verify' => false,
]);
Route::get('/logout', 'Auth\LoginController@logout')->name('get-logout');

Route::group([
    'middleware' => 'auth',
    'namespace' => 'Profile',
], function () {
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::get('/profile/ads/{category}', 'ProfileController@user_ads_by_category')->name('user_ads_by_category');

    Route::resource('ads', 'AdController')->except(['index', 'show']);
});

Route::get('/', 'MainController@index')->name('index');
Route::get('/how_is_it_made', 'MainController@how_is_it_made')->name('how_is_it_made');
Route::get('/obyavlenie', 'MainController@ads')->name('ads');
Route::get('/obyavlenie/{category}', 'MainController@ads_by_category')->name('ads_by_category');
Route::get('/obyavlenie/{category}/{ad_id}', 'MainController@ad_id')->name('ad_id');
Route::get('/user_ads/{user_id}', 'MainController@user_ads')->name('user_ads');
